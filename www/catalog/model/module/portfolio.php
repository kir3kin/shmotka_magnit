<?php
class ModelModulePortfolio extends Model {
	public function get() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "portfolio_image ORDER BY portfolio_image_id, sort_order");
		return $query->rows;
	}
}
