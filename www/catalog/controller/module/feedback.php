<?php
class ControllerModuleFeedback extends Controller {
    public function index() {
        $this->load->model('module/feedback');
        $feedback_entries = $this->model_module_feedback->getApproved();

        $data['feedback_block_header'] = 'Отзывы';
        $data['feedback_entries'] = array();

        foreach ($feedback_entries as $feedback_entry) {
			$dt = explode(' ', $feedback_entry['timestamp']);
			$tm = $dt[1];

			$dt = explode('-', $dt[0]);
			$dt = $dt[2].'.'.$dt[1].'.'.$dt[0];

			$data['feedback_entries'][] = array(
				'author' => $feedback_entry['author'],
				'dt' => $dt.' '.$tm,
				'text' => $feedback_entry['text'],
				'photo' => $feedback_entry['photo']
			);
		}

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/feedback.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/feedback.tpl', $data);
        } else {
			return $this->load->view('default/template/module/feedback.tpl', $data);
        }
    }
}
