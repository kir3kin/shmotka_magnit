<?php
class ControllerModuleLeedform extends Controller {
    public function index() {
        $data['leedform_header'] = $this->config->get('leedform_header');
        $data['leedform_description'] = $this->config->get('leedform_description');
        $data['leedform_upload'] = $this->config->get('leedform_upload');
        $data['leedform_upload_text'] = $this->config->get('leedform_upload_text');
        $data['leedform_pc'] = $this->config->get('leedform_pc');
        $data['leedform_mob'] = $this->config->get('leedform_mob');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/leedform.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/leedform.tpl', $data);
        } else {
            return $this->load->view('default/template/module/leedform.tpl', $data);
        }
    }
}
