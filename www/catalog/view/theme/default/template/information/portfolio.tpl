<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
	    <?php echo $content_top; ?>
	    <h1><?php echo $heading_title; ?></h1>
		<p><?php echo $heading_info; ?></p>

		<style>
			.img_block { display: inline-block; width: 23%; height: 200px; margin: 0 5px 5px 0; overflow: hidden; vertical-align: top; }
			.img_wrap { display: block; height: 200px; line-height: 190px; overflow: hidden; text-align: center; }
			.img_el { width: 95%; }
			.mfp-image-holder .mfp-content { max-width: 80%; }
		</style>

		<div id="portfolio_images">
			<?php foreach ($portfolio_images as $portfolio_image) { ?>
				<div id="img_block_<?php echo $portfolio_image['id']; ?>" class="img_block">
					<a class="img_wrap" href="<?php echo $portfolio_image['path']; ?>"><img class="img_el" src="<?php echo $portfolio_image['path']; ?>"></a>
				</div>
			<?php } ?>
		</div>

		<script type="text/javascript">
			$(document).ready(function() {
				$('.img_wrap').magnificPopup({
					type: 'image',
					gallery: { enabled: true }
				});
			});
		</script>

	    <?php echo $content_bottom; ?>
	</div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>
