<div class="list-group">
<?php if (0) {  // свёрнутый режим?>
	  <?php foreach ($categories as $category) { ?>
	 	 	<?php if ($category['category_id'] == $category_id) { ?>
	  		<a href="<?php echo $category['href']; ?>" class="list-group-item active"><b><?php echo mb_strtoupper($category['name'], 'UTF-8'); ?></b></a>

	 	 		<?php if ($category['children']) { ?>
	   				<?php foreach ($category['children'] as $child) { ?>
	  					<?php if ($child['category_id'] == $child_id) { ?>
	 						 <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
	  					<?php } else { ?>
	  						<a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
						<?php } ?>
	 				<?php } ?>
	 			<?php } ?>

	 		 <?php } else { ?>
	  				<a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo mb_strtoupper($category['name'], 'UTF-8'); ?></a>

	  			<?php if ($category['children']) { ?>
	   				<?php foreach ($category['children'] as $child) { ?>
	  					<?php if ($child['category_id'] == $child_id) { ?>
	  						<a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
						<?php } ?>
	 				<?php } ?>
	 			<?php } ?>

	  		 <?php } ?>
	  <?php } ?>
  <?php } else { // развёрнутый режим?>
	  <?php foreach ($categories as $category) { ?>
	 	 	<?php if ($category['category_id'] == $category_id) { ?>
	  			<a href="<?php echo $category['href']; ?>" class="list-group-item active"><b><?php echo mb_strtoupper($category['name'], 'UTF-8'); ?></b></a>
	  		<?php } else { ?>
	  			<a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo mb_strtoupper($category['name'], 'UTF-8'); ?></a>
	 		<?php } ?>

	   				<?php foreach ($category['children'] as $child) { ?>
	  					<?php if ($child['category_id'] == $child_id) { ?>
	 						 <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
	  					<?php } else { ?>
	  						<a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
						<?php } ?>
	 				<?php } ?>
	  		 <?php } ?>
  <?php } ?>
</div>
