<div>
	<style>
		.feedback_block_header { margin-top: 20px; padding: 8px 12px; color: #fff; background: #5b7fa6; border-top-right-radius: 4px; border-top-left-radius: 4px; text-transform: uppercase; }
		.feedback_btn { display: block; width: 120px; margin: 14px 12px 6px; background: #5b7fa6; border-color: #547599; }
		.feedback_btn:hover { background: #5b7fa6; border-color: #547599; opacity: 0.9; }
		.feedback_entry { padding: 8px 12px; border: 1px solid #bbb; border-left-width: 0; border-right-width: 0; border-top-width: 0; }
		.feedback_photo { width: 50px; height: 50px; }
		.feedback_caption { display: inline-block; vertical-align: middle; }
		.feedback_author { font-weight: bold; }
		.feedback_dt { font-size: 11px; }
		.feedback_text { font-size: 12px; }
	</style>

	<div class="feedback_block_header"><?php echo $feedback_block_header; ?></div>
	<a href="<?php echo HTTP_SERVER; ?>index.php?route=information/feedback" class="btn btn-primary feedback_btn">Добавить отзыв</a>

	<?php foreach ($feedback_entries as $feedback_entry) { ?>
		<div class="feedback_entry">
			<?php if ($feedback_entry['photo']) { ?>
				<img class="feedback_photo" src="<?php echo $feedback_entry['photo']; ?>" />
			<?php } ?>

			<div class="feedback_caption">
				<div class="feedback_author"><?php echo $feedback_entry['author']; ?></div>
				<div class="feedback_dt"><?php echo $feedback_entry['dt']; ?></div>
			</div>

			<div class="feedback_text"><?php echo $feedback_entry['text']; ?></div>
		</div>
	<?php } ?>
</div>
