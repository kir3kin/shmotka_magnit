<style>
	.leed_form_div { display: none; margin-top: 20px; padding: 20px 20px 25px 20px; background: #424242; text-align: center; }
	.leed_form { width: 70%; margin: 0 auto; }
	.leed_form_header { color: #fff; font-size: 24px; font-weight: bold; line-height: 24px; }
	.leed_form_description { margin: 30px; color: #cecbce; font-size: 16px; }
	.upload_image_wrapper { margin-bottom: 20px; text-align: left; }
	.upload_image_button { width: 65px; padding: 2px; font-size: 13px; }
	.upload_image_label { color: #fff; vertical-align: middle; font-size: 13px; }
	.leed_form_image { display: none; width: 50%; margin-bottom: 5px; }
	.leed_form_button { background: #fcf415; border-color: #d3cd0c; color: #1b1b1b; font-size: 16px; }
	.leed_form_button:hover { opacity: 0.9; }

	<?php if ($leedform_pc == '1') echo '.leed_form_div { display: block; }'; ?>

	@media (max-width: 767px) {
		.leed_form_div { display: none; }
		.leed_form_header { font-size: 22px; }
		.leed_form_description { font-size: 14px; }
		<?php if ($leedform_mob == '1') echo '.leed_form_div { display: block; }'; ?>
	}
</style>

<div class="leed_form_div">
	<div class="leed_form_header"><?php echo $leedform_header; ?></div>
	<div class="leed_form_description"><?php echo $leedform_description; ?></div>

	<div class="leed_form">
		<input type="text" class="form-control" style="margin-bottom: 30px;" name="leedform_name" value="" placeholder="Введите Ваше имя" />
		<input type="text" maxlength="20" class="form-control" style="margin-bottom: 25px;" name="leedform_phone" value="" placeholder="Введите Ваш телефон" />
		<input id="upload_image_url" type="hidden" name="leedform_image" value="" />

		<?php if ($leedform_upload == '1') { ?>
			<div class="upload_image_wrapper">
				<input id="upload_image_btn" type="button" class="btn upload_image_button" value="Обзор" />
				<span class="upload_image_label">&nbsp;<?php echo $leedform_upload_text; ?></span>
			</div>

			<img id="upload_image_img" src="" class="leed_form_image" />
		<?php } ?>

		<input id="leedform_order_btn" class="btn leed_form_button" type="button" value="Отправить заявку" />
	</div>

	<?php if ($leedform_upload == '1') { require(DIR_TEMPLATE.'default/template/common/imageuploader.tpl'); ?>
		<script type="text/javascript">
			var img = document.getElementById('upload_image_img');

			img.style.display = img.getAttribute('src') == '' ? '' : 'block';
			$("input[name='leedform_phone']").keypress(testPhoneInput);

			function imageUploaded(json) {
				if (json['error']) alert(json['error']);

				if (json['success']) {
					var src = json['img_url'];

					img.src = document.getElementById('upload_image_url').value = src;
					img.style.display = 'block';
				}
			}

			addUploadHandler(document.getElementById('upload_image_btn'), imageUploaded);
		</script>
	<?php } ?>

	<script type="text/javascript">
		$('#leedform_order_btn').click(function() {
			var order_info = {
					product_id: '1743',
					firstname: $("input[name='leedform_name']")[0].value,
					telephone: $("input[name='leedform_phone']")[0].value,
					order_image: document.getElementById('upload_image_url').value,
					address: '---',
					comment: ''
				};

			if (order_info.firstname == '' || order_info.firstname.length > 32) { alert('Имя должно быть от 1 до 32 символов!'); return; }
			if (order_info.telephone == '') { alert('Номер телефона не соответствует заданному шаблону!'); return; }
			if (order_info.order_image == '') delete order_info.order_image;

			$.ajax({
	            url: 'index.php?route=checkout/buy/buy_now',
	            type: 'post',
	            dataType: 'json',
	            data: order_info,

	            success: function(data) {
					if (data.success == '1') location = 'index.php?route=checkout/success';
					else console.log(data);
				},

				error: function(xhr, opt, err) { alert(err); }
	        });
		});
	</script>
</div>
