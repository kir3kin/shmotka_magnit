<div  style="text-align: center;">
	<div style="padding: 20px; height: 170px; margin-top: 20px; background-color: rgb(229, 229, 229);">
		<a href="<?php echo HTTP_SERVER; ?>index.php?route=product/special">
		  <p class="atimer_label_text"><?php echo $atimer_label_text; ?></p>
		  <br><link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet"><link href="timer/css/styles.css" rel="stylesheet">
		  <link href="timer/countdown/jquery.countdown.css" rel="stylesheet">
		  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		  <div id="countdown"></div>
		</a>
	</div>

	<script src="timer/countdown/jquery.countdown.js"></script>

	<script type="text/javascript">
		( function() {
			var start = new Date(<?php echo $atimer_year; ?>, Number(<?php echo $atimer_mon; ?>) - 1, <?php echo $atimer_day; ?>, <?php echo $atimer_hour; ?>, <?php echo $atimer_min; ?>).getTime(),
				period = (Number(<?php echo $atimer_period_day; ?>) * 86400 + Number(<?php echo $atimer_period_hour; ?>) * 3600 + Number(<?php echo $atimer_period_min; ?>) * 60) * 1000;

			function initTimestamp() {
				var dt = Date.now(), tm = dt - start,
					stamp = tm > period ? period - tm % period : period - tm;

				return dt + stamp;
			}

			function tick(d, h, m, s) {
				if (d == 0 && h == 0 && m == 0 && s == 0) this.timestamp = initTimestamp();
			}

			var ts = initTimestamp();
			$('#countdown').countdown({ timestamp: ts, callback: tick });
		}() );
	</script>

	<div style="clear: both;"></div>
</div>
