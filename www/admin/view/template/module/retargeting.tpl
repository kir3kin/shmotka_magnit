<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-retargeting" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<style>
			.st_div { margin-top: 50px; }
		</style>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-retargeting" class="form-horizontal">
	        <div class="form-group">
	            <label class="col-sm-2 control-label"><?php echo $entry_yandex; ?></label>

	            <div class="col-sm-10">
					<textarea class="form-control" name="retargeting_yandex" rows="10"><?php echo $retargeting_yandex; ?></textarea>
	            </div>
	        </div>

	        <div class="form-group">
	            <label class="col-sm-2 control-label"><?php echo $entry_vk; ?></label>

	            <div class="col-sm-10">
					<textarea class="form-control" name="retargeting_vk" rows="10"><?php echo $retargeting_vk; ?></textarea>
	            </div>
	        </div>

	        <div class="form-group">
	            <label class="col-sm-2 control-label"><?php echo $entry_vk_success; ?></label>

	            <div class="col-sm-10">
					<textarea class="form-control" name="retargeting_vk_success" rows="10"><?php echo $retargeting_vk_success; ?></textarea>
	            </div>
	        </div>

	        <div class="form-group st_div">
	            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	            <div class="col-sm-10">
	              <select name="retargeting_status" id="input-status" class="form-control">
	                <?php if ($retargeting_status) { ?>
	                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
	                <option value="0"><?php echo $text_disabled; ?></option>
	                <?php } else { ?>
	                <option value="1"><?php echo $text_enabled; ?></option>
	                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
	                <?php } ?>
	              </select>
	            </div>
	        </div>
        </form>
      </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
