<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table id="module" class="list">
          <thead>
            <tr>
	          <td class="left"><?php echo $entry_categories; ?></td>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $module_row = 0; ?>
          <?php foreach ($modules as $module) { ?>
          <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td class="left">
              <table border="0" width="100%" class="categories">
              	<tr>
					<td>
		              <select class="categories-list">
		   	              <?php foreach ($categories_list as $category) { ?>
		                  <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
		                  <?php } ?>
	                	</select>
	                	<input type="button" value="Add" onclick="addCategory(<?php echo $module_row; ?>)"/>
	                </td>
	             </tr>
	             <?php foreach ($module['categories'] as $key => $category){ ?>
	             <tr class="category_<?php echo $key; ?>">
		            <td><input type="hidden" value="<?php echo $category['name']; ?>" name="category_sales_module[<?php echo $module_row; ?>][categories][<?php echo $key; ?>][name]"><?php echo $category['name'];?></td>
	             	<td>
		             	<div class="image"><img src="<?php echo $category['thumb']; ?>" alt="" id="thumb-<?php echo $module_row; ?>-<?php echo $key; ?>" />
	                    <input type="hidden" name="category_sales_module[<?php echo $module_row; ?>][categories][<?php echo $key; ?>][image]" value="<?php echo $category['image']; ?>" id="image-<?php echo $module_row; ?>-<?php echo $key; ?>" />
	                    <br />
	                    <a onclick="image_upload('image-<?php echo $module_row; ?>-<?php echo $key; ?>', 'thumb-<?php echo $module_row; ?>-<?php echo $key; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb-<?php echo $module_row; ?>-<?php echo $key; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image-<?php echo $module_row; ?>-<?php echo $key; ?>').attr('value', '');"><?php echo $text_clear; ?></a></div>
                    </td>
                    <td><select name="category_sales_module[<?php echo $module_row; ?>][categories][<?php echo $key; ?>][price]">
                    	<option value="0"<?php if($category['price'] == 0){ ?> selected <?php } ?>><?php echo $text_price_hidden; ?></option>
                    	<option value="1"<?php if($category['price'] == 1){ ?> selected <?php } ?>><?php echo $text_price_min; ?></option>
                    	<option value="2"<?php if($category['price'] == 2){ ?> selected <?php } ?>><?php echo $text_price_max; ?></option>
                    </select></td>
                    <td><a onclick="removeCategory(<?php echo $module_row; ?>, <?php echo $key; ?>)"><?php echo $button_remove; ?></a></td>
	             </tr>
	             <?php } ?>
				</table>

                </td>
              <td class="left"><select name="category_sales_module[<?php echo $module_row; ?>][layout_id]">
                  <?php foreach ($layouts as $layout) { ?>
                  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="category_sales_module[<?php echo $module_row; ?>][position]">
                  <?php if ($module['position'] == 'content_top') { ?>
                  <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                  <?php } else { ?>
                  <option value="content_top"><?php echo $text_content_top; ?></option>
                  <?php } ?>
                  <?php if ($module['position'] == 'content_bottom') { ?>
                  <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                  <?php } else { ?>
                  <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                  <?php } ?>
                  <?php if ($module['position'] == 'column_left') { ?>
                  <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                  <?php } else { ?>
                  <option value="column_left"><?php echo $text_column_left; ?></option>
                  <?php } ?>
                  <?php if ($module['position'] == 'column_right') { ?>
                  <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                  <?php } else { ?>
                  <option value="column_right"><?php echo $text_column_right; ?></option>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="category_sales_module[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              <td class="right"><input type="text" name="category_sales_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
            </tr>
          </tbody>
          <?php $module_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="5"></td>
              <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function addCategory(row_id){	var category_name = $("#module-row"+row_id+" select.categories-list option:selected").text();
	var category_id = $("#module-row"+row_id+" select.categories-list option:selected").val();

    var html  = '<tr class="category_'+category_id+'">';
    html += '     <td><input type="hidden" value="'+category_name+'" name="category_sales_module['+row_id+'][categories]['+category_id+'][name]">'+category_name+'</td>';
    html += '     	<td>';
    html += '      	<div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb-'+row_id+'-'+category_id+'" />';
    html += '            <input type="hidden" name="category_sales_module['+row_id+'][categories]['+category_id+'][image]" value="" id="image-'+row_id+'-'+category_id+'" />';
    html += '            <br />';
    html += '            <a onclick="image_upload(\'image-'+row_id+'-'+category_id+'\', \'thumb-'+row_id+'-'+category_id+'\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb-'+row_id+'-'+category_id+'\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image-'+row_id+'-'+category_id+'\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div>';
    html += '           </td>';
    html += '     <td><a onclick="removeCategory('+row_id+', '+category_id+')"><?php echo $button_remove; ?></a></td>';
    html += '     </tr>';
	$("#module-row"+row_id+" table.categories").append(html);}
function removeCategory(row_id, cat_id){	$("#module-row"+row_id+" tr.category_"+cat_id).remove();}
var module_row = <?php echo $module_row; ?>;

function addModule() {
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
    html += '   <td class="left">';
    html += '	<table class="categories">';
    html += '		<tr>';
    html += '			<td>';
	html += '	      <select class="categories-list">';
	<?php foreach ($categories_list as $category) { ?>
	html += '	         <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>';
	<?php } ?>
	html += '          </select>';
	html += '          <input type="button" value="Add" onclick="addCategory('+module_row+')"/>';
	html += '			</td>';
	html += '		</tr>';
	html += '	</table>';
    html += '    </td>';
	html += '    <td class="left"><select name="category_sales_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="category_sales_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="category_sales_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="category_sales_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';

	$('#module tfoot').before(html);

	module_row++;
}
//--></script>
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();

	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

	$('#dialog').dialog({
		title: 'ImageManager',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script>
<?php echo $footer; ?>