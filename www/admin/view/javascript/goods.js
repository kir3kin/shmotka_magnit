$(document).ready(function() {
	var $goodElms = $(".info_goods");
	for (var i = 0; i < $goodElms.length; i++) {
		if (i !== $goodElms.length -1) $($goodElms[i]).text($($goodElms[i]).text() + ", ");
	}

	$(".copyPast").val($(".info_goods").text());//copyPast

	$(".btn-clip-good").hover(function() {
		$(this).css("text-shadow", "0 0 5px rgba(0,0,0,.5)");
	}, function() {
		$(this).css("text-shadow", "0 0 0 rgba(0,0,0,0)");
	});

	$(".btn-clip-good").click(function() {
		$(this).css("color", "#000");
	});
});