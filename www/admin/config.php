<?php
// HTTP
$host = $_SERVER['HTTP_HOST'];
define('HTTP_SERVER', 'http://'.$host.'/admin/');
define('HTTP_CATALOG', 'http://'.$host.'/');

// HTTPS
define('HTTPS_SERVER', 'http://'.$host.'/admin/');
define('HTTPS_CATALOG', 'http://'.$host.'/');

// DIR
define('DIR_APPLICATION', 'Z:/home/oc_shmotka.loc/www/admin/');
define('DIR_SYSTEM', 'Z:/home/oc_shmotka.loc/www/system/');
define('DIR_LANGUAGE', 'Z:/home/oc_shmotka.loc/www/admin/language/');
define('DIR_TEMPLATE', 'Z:/home/oc_shmotka.loc/www/admin/view/template/');
define('DIR_CONFIG', 'Z:/home/oc_shmotka.loc/www/system/config/');
define('DIR_IMAGE', 'Z:/home/oc_shmotka.loc/www/image/');
define('DIR_CACHE', 'Z:/home/oc_shmotka.loc/www/system/storage/cache/');
define('DIR_DOWNLOAD', 'Z:/home/oc_shmotka.loc/www/system/storage/download/');
define('DIR_LOGS', 'Z:/home/oc_shmotka.loc/www/system/storage/logs/');
define('DIR_MODIFICATION', 'Z:/home/oc_shmotka.loc/www/system/storage/modification/');
define('DIR_UPLOAD', 'Z:/home/oc_shmotka.loc/www/system/storage/upload/');
define('DIR_CATALOG', 'Z:/home/oc_shmotka.loc/www/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'alex');
define('DB_PASSWORD', '1234');
define('DB_DATABASE', 'oc_shmotka_magnit');
define('DB_PORT', '3306');
define('DB_PREFIX', 'tr_');
