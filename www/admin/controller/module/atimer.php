<?php
class ControllerModuleatimer extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('module/atimer');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        // Start If: Validates and check if data is coming by save (POST) method
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('atimer', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        // Assign the language data for parsing it to view
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit']    = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_content_top'] = $this->language->get('text_content_top');
        $data['text_content_bottom'] = $this->language->get('text_content_bottom');
        $data['text_column_left'] = $this->language->get('text_column_left');
        $data['text_column_right'] = $this->language->get('text_column_right');

        $data['entry_label'] = $this->language->get('entry_label');
        $data['entry_date'] = $this->language->get('entry_date');
        $data['entry_period'] = $this->language->get('entry_period');
		$data['entry_year'] = $this->language->get('entry_year');
		$data['entry_mon'] = $this->language->get('entry_mon');
		$data['entry_day'] = $this->language->get('entry_day');
		$data['entry_hour'] = $this->language->get('entry_hour');
		$data['entry_min'] = $this->language->get('entry_min');
		$data['entry_lbl'] = $this->language->get('entry_lbl');

        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['entry_position'] = $this->language->get('entry_position');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_add_module'] = $this->language->get('button_add_module');
        $data['button_remove'] = $this->language->get('button_remove');

        // This Block returns the warning if any
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        // This Block returns the error code if any
        if (isset($this->error['code'])) {
            $data['error_code'] = $this->error['code'];
        } else {
            $data['error_code'] = '';
        }

        // Making of Breadcrumbs to be displayed on site
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/atimer', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('module/atimer', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['atimer_label_text'])) {
            $data['atimer_label_text'] = $this->request->post['atimer_label_text'];
        } else {
            $data['atimer_label_text'] = $this->config->get('atimer_label_text');
        }

		if (isset($this->request->post['atimer_year'])) {
            $data['atimer_year'] = $this->request->post['atimer_year'];
        } else {
            $data['atimer_year'] = $this->config->get('atimer_year');
        }

		if (isset($this->request->post['atimer_mon'])) {
            $data['atimer_mon'] = $this->request->post['atimer_mon'];
        } else {
            $data['atimer_mon'] = $this->config->get('atimer_mon');
        }

		if (isset($this->request->post['atimer_day'])) {
            $data['atimer_day'] = $this->request->post['atimer_day'];
        } else {
            $data['atimer_day'] = $this->config->get('atimer_day');
        }

		if (isset($this->request->post['atimer_hour'])) {
            $data['atimer_hour'] = $this->request->post['atimer_hour'];
        } else {
            $data['atimer_hour'] = $this->config->get('atimer_hour');
        }

		if (isset($this->request->post['atimer_min'])) {
            $data['atimer_min'] = $this->request->post['atimer_min'];
        } else {
            $data['atimer_min'] = $this->config->get('atimer_min');
        }

        if (isset($this->request->post['atimer_period_day'])) {
            $data['atimer_period_day'] = $this->request->post['atimer_period_day'];
        } else {
            $data['atimer_period_day'] = $this->config->get('atimer_period_day');
        }

		if (isset($this->request->post['atimer_period_hour'])) {
            $data['atimer_period_hour'] = $this->request->post['atimer_period_hour'];
        } else {
            $data['atimer_period_hour'] = $this->config->get('atimer_period_hour');
        }

		if (isset($this->request->post['atimer_period_min'])) {
            $data['atimer_period_min'] = $this->request->post['atimer_period_min'];
        } else {
            $data['atimer_period_min'] = $this->config->get('atimer_period_min');
        }

        // This block parses the status (enabled / disabled)
        if (isset($this->request->post['atimer_status'])) {
            $data['atimer_status'] = $this->request->post['atimer_status'];
        } else {
            $data['atimer_status'] = $this->config->get('atimer_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/atimer.tpl', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/atimer')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) return true; else return false;
    }
}
