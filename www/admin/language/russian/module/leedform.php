<?php
// Heading
$_['heading_title']       = 'Лид форма';

// Text
$_['text_module']         = 'Модули';
$_['text_edit']           = 'Настройки модуля';
$_['text_success']        = 'Настройки успешно изменены!';
$_['text_enabled']        = 'Включено';
$_['text_disabled']       = 'Отключено';

$_['entry_status']        = 'Статус:';

// Error
$_['error_permission']    = 'Внимание: Недостаточно полномочий для изменения модуля!';
$_['error_code']          = 'Обязательный параметр';
