<?php
// Heading
$_['heading_title']       = 'Отзывы';

// Text
$_['text_module']         = 'Модули';
$_['text_edit']           = 'Настройки модуля';
$_['text_success']        = 'Настройки успешно изменены!';
$_['text_enabled']        = 'Включено';
$_['text_disabled']       = 'Отключено';

$_['entry_dt']            = 'Дата:';
$_['entry_author']        = 'Автор:';
$_['entry_text']          = 'Текст:';
$_['entry_status']        = 'Статус:';

$_['button_update']        = 'Обновить';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module!';
$_['error_code']          = 'Обязательный параметр';
