<?php
// Heading
$_['heading_title']    			= 'Extras';

// Text
$_['text_module']      			= 'Modules';
$_['text_success']     			= 'Success: You have modified module Extras!';
$_['text_edit']					= 'Edit Module Extras';

// Entry
$_['entry_status']   			= 'Status'; 

// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify module Extras!';

// Mail
$_['button_review']					  	   = 'Rate Extras';
$_['tab_about']							   = 'About';
$_['text_need_support']	  				   = 'No support available for this extension.';
$_['text_support']		  				   = 'Support';
$_['text_follow']		  				   = 'Follow Us';