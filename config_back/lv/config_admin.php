<?php
// HTTP
$host = $_SERVER['HTTP_HOST'];
define('HTTP_SERVER', 'http://'.$host.'/admin/');
define('HTTP_CATALOG', 'http://'.$host.'/');

// HTTPS
define('HTTPS_SERVER', 'http://'.$host.'/admin/');
define('HTTPS_CATALOG', 'http://'.$host.'/');

// DIR
define('DIR_APPLICATION', '/home/webtech/web-tehnology.com/lv/admin/');
define('DIR_SYSTEM', '/home/webtech/web-tehnology.com/lv/system/');
define('DIR_LANGUAGE', '/home/webtech/web-tehnology.com/lv/admin/language/');
define('DIR_TEMPLATE', '/home/webtech/web-tehnology.com/lv/admin/view/template/');
define('DIR_CONFIG', '/home/webtech/web-tehnology.com/lv/system/config/');
define('DIR_IMAGE', '/home/webtech/web-tehnology.com/lv/image/');
define('DIR_CACHE', '/home/webtech/web-tehnology.com/lv/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/webtech/web-tehnology.com/lv/system/storage/download/');
define('DIR_LOGS', '/home/webtech/web-tehnology.com/lv/system/storage/logs/');
define('DIR_MODIFICATION', '/home/webtech/web-tehnology.com/lv/system/storage/modification/');
define('DIR_UPLOAD', '/home/webtech/web-tehnology.com/lv/system/storage/upload/');
define('DIR_CATALOG', '/home/webtech/web-tehnology.com/lv/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'webtech.mysql.ukraine.com.ua');
define('DB_USERNAME', 'webtech_lv');
define('DB_PASSWORD', 'nu5lgsj2');
define('DB_DATABASE', 'webtech_lv');
define('DB_PORT', '3306');
define('DB_PREFIX', 'tr_');
